const path = require('path')

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? `/${process.env.CI_PROJECT_NAME}/`
    : '/',

  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src')
      },
      extensions: ['.js', '.vue', '.json', '.sass']
    }
  }
}
